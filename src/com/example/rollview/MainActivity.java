package com.example.rollview;

import java.util.ArrayList;
import java.util.List;


import android.os.Bundle;
import android.app.Activity;

public class MainActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 10; i++){
			list.add(Integer.toString(i));
		}
		
		RollView roll = (RollView) findViewById(R.id.roll_a);
		roll.setList(list);
		
	}
}
